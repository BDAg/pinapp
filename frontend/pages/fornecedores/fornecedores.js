// GET para listar funcionarios
var request = new XMLHttpRequest();
request.open("GET", "https://api-pinapp.herokuapp.com/fornecedores", true);
request.onload = function () {
  var data = JSON.parse(this.response);
  if (request.status >= 200 && request.status < 400) {
    var tbl =
      "<th>Código</th><th>Nome</th><th>Telefone</th><th>E-mail</th><th>Empresa</th><th>Endereço</th><th>Cidade</th><th>Estado</th><th>Ferramentas</th>";
    for (var i in data) {
      tbl +=
        "<tr><td >" +
        data[i].id +
        "</td><td>" +
        data[i].nome +
        "</td><td>" +
        data[i].telefone +
        "</td><td>" +
        data[i].email +
        "</td><td>" +
        data[i].empresa_nome +
        "</td><td>" +
        data[i].address +
        "</td><td>" +
        data[i].city +
        "</td><td>" +
        data[i].uf +
        '</td><td><i onClick="formEdicao(' +
        data[i].id +
        ')" class="fas fa-edit fa-lg" data-toggle="modal" data-target="#cadastroModal2"></i><i onClick="confirmarRemover(' +
        data[i].id +
        ')" class="fas fa-trash-alt fa-lg" data-toggle="modal" data-target="#cadastroModal3"></i></td></tr>';
    }
    document.getElementById("tabela").innerHTML = tbl;
  } else {
    const errorMessage = document.createElement("marquee");
    errorMessage.textContent = "erro";
    app.appendChild(errorMessage);
  }
};
request.send();

function confirmarRemover(id) {
  var txt = "";
  txt +=
    ' <form name="form1" class="needs-validation" novalidate><div class="form-row"><label class="alert" for="validationCustom01">Tem certeza que deseja remover esse fornecedor?</label></div><div id="alert3"></div></form><button class="btn btn-primary" onClick="remover(' +
    id +
    ')" id="btn" type="submit">Remover</button> ';
  document.getElementById("remover").innerHTML = txt;
}

// função para criar form de edição
function formEdicao(id) {
  var request = new XMLHttpRequest();
  request.open(
    "GET",
    "https://api-pinapp.herokuapp.com/fornecedores/" + id,
    true
  );
  request.onload = function () {
    var data = JSON.parse(this.response);
    if (request.status >= 200 && request.status < 400) {
      var txt = "";
      txt +=
        ' <form name="form2" class="needs-validation" novalidate><div class="form-row"> <div class="col-md-4 mb-3"> <label for="validationCustom01">Nome</label> <input name="nome" type="text" class="form-control" id="validationCustom01" placeholder="" value="' +
        data[0].nome +
        '" required> <div class="valid-feedback"> Correto!  </div> <div class="invalid-feedback"> Esse não campo não pode permanecer vazio.  </div> </div> <div class="col-md-4 mb-3"> <label for="validationCustom02">Telefone</label> <input name="telefone" type="text" class="form-control" id="validationCustom02" placeholder="" value="' +
        data[0].telefone +
        '" required> <div class="valid-feedback"> Correto!  </div> <div class="invalid-feedback"> Esse não campo não pode permanecer vazio.  </div> </div> <div class="col-md-4 mb-3"> <label for="validationCustom03">E-mail</label> <input name="email" type="text" class="form-control" id="validationCustom03" placeholder="" value="' +
        data[0].email +
        '" required> <div class="valid-feedback"> Correto!  </div> <div class="invalid-feedback"> Esse não campo não pode permanecer vazio.  </div> </div> </div> <div class="form-row" style="margin-bottom: 10px;"> <div class="col-md-6 mb-6"> <label for="validationCustom04">Empresa</label> <input name="empresa" type="text" class="form-control" id="validationCustom04" value="' +
        data[0].empresa_nome +
        '" placeholder="" required> <div class="valid-feedback"> Correto!  </div> <div class="invalid-feedback"> Esse não campo não pode permanecer vazio.  </div> </div> <div class="col-md-6 mb-6"> <label for="validationCustom05">Endereço</label> <input name="endereco" type="text" class="form-control" id="validationCustom05" value="' +
        data[0].address +
        '" placeholder="" required> <div class="valid-feedback"> Correto!  </div> <div class="invalid-feedback"> Esse não campo não pode permanecer vazio.  </div> </div> </div> <div class="form-row"> <div class="col-md-6 mb-6"> <label for="validationCustom06">Cidade</label> <input name="cidade" type="text" class="form-control" id="validationCustom06" value="' +
        data[0].city +
        '" placeholder="" required> <div class="valid-feedback"> Correto!  </div> <div class="invalid-feedback"> Esse não campo não pode permanecer vazio.  </div> </div> <div class="col-md-6 mb-6"> <label for="validationCustom07">Estado</label> <input name="estado" type="text" class="form-control" id="validationCustom07" value="' +
        data[0].uf +
        '" placeholder="" required> <div class="valid-feedback"> Correto!  </div> <div class="invalid-feedback"> Esse não campo não pode permanecer vazio.  </div> </div> </div> <div class="form-group"> <div class="form-check"> <input class="form-check-input" type="checkbox" value="" id="invalidCheck2" required> <label class="form-check-label" for="invalidCheck"> Tem certeza que deseja editar esse fornecedor?  </label> <div class="invalid-feedback"> Para concluir selecione essa opção!  </div> </div> </div> </form><div id="alert2"></div> <button onClick="editar(' +
        id +
        ')" class="btn btn-primary" id="btn" type="submit">Cadastrar</button>  ';
      document.getElementById("update").innerHTML = txt;
    } else {
      const errorMessage = document.createElement("marquee");
      errorMessage.textContent = "erro";
      app.appendChild(errorMessage);
    }
  };
  request.send();
}

// edita fornecedor no banco de dados
function editar(id) {
  if (
    document.form2.nome.value == "" ||
    document.form2.telefone.value == "" ||
    document.form2.email.value == "" ||
    document.form2.empresa.value == "" ||
    document.form2.endereco.value == "" ||
    document.form2.cidade.value == "" ||
    document.form2.estado.value == "" ||
    document.getElementById("invalidCheck2").checked == false
  ) {
    document.getElementById("alert2").innerHTML =
      '<div class="alert">Informe todos os dados corretamente</div>';
  } else {
    fetch("https://api-pinapp.herokuapp.com/fornecedores/" + id, {
      method: "PUT",
      body: JSON.stringify({
        nome: "" + document.form2.nome.value + "",
        telefone: "" + document.form2.telefone.value + "",
        email: "" + document.form2.email.value + "",
        empresa_nome: "" + document.form2.empresa.value + "",
        address: "" + document.form2.endereco.value + "",
        city: "" + document.form2.cidade.value + "",
        uf: "" + document.form2.estado.value + "",
      }),
      headers: { "Content-type": "application/json; charset=UTF-8" },
    }).then((res) => atualizar(res.json())); // or res.json()
    document.getElementById("alert2").innerHTML = '<div class="spinner"></div>';
  }
}

// deletar um fornecedor
function remover(id) {
  fetch("https://api-pinapp.herokuapp.com/fornecedores/" + id, {
    method: "DELETE",
  })
    .then((res) => res.text()) // or res.json()
    .then((res) => atualizar(res));
  document.getElementById("alert3").innerHTML = '<div class="spinner"></div>';
}

function novo() {
  if (
    document.form1.nome.value == "" ||
    document.form1.telefone.value == "" ||
    document.form1.email.value == "" ||
    document.form1.empresa.value == "" ||
    document.form1.endereco.value == "" ||
    document.form1.cidade.value == "" ||
    document.form1.estado.value == "" ||
    document.getElementById("invalidCheck").checked == false
  ) {
    document.getElementById("alert").innerHTML =
      '<div class="alert">Informe todos os dados corretamente</div>';
  } else {
    fetch("https://api-pinapp.herokuapp.com/fornecedores", {
      method: "POST",

      body: JSON.stringify({
        nome: "" + document.form1.nome.value + "",
        telefone: "" + document.form1.telefone.value + "",
        email: "" + document.form1.email.value + "",
        empresa_nome: "" + document.form1.empresa.value + "",
        address: "" + document.form1.endereco.value + "",
        city: "" + document.form1.cidade.value + "",
        uf: "" + document.form1.estado.value + "",
      }),
      headers: { "Content-type": "application/json; charset=UTF-8" },
    })
      .then((res) => res.json()) // or res.json()
      .then((json) => atualizar(json));
    document.getElementById("alert").innerHTML = '<div class="spinner"></div>';
  }
}

// atualizar a página
function atualizar() {
  window.location.href = "fornecedores.html";
  document.getElementById("alert").innerHTML = "";
}


/////////////////////////////



$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
});

document.querySelectorAll('.table-fornecedores tr').forEach(function (elem) {
    if (parseFloat(window.getComputedStyle(elem).width) === parseFloat(window.getComputedStyle(elem.parentElement).width)) {
        elem.setAttribute('title', elem.textContent);
    }
});

function search() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("buscador");
    filter = input.value.toUpperCase();
    table = document.getElementById("tabela");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[4];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

(function () {
    'use strict';
    window.addEventListener('load', function () {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

function addRow() {

    var nome = document.getElementById("validationCustom01");
    var telefone = document.getElementById("validationCustom02");
    var email = document.getElementById("validationCustom03");
    var empresa = document.getElementById("validationCustom04");
    var endereco = document.getElementById("validationCustom05");
    var cidade = document.getElementById("validationCustom06");
    var estado = document.getElementById("validationCustom07");

    var table = document.getElementById("tabela");


    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);

    row.insertCell(0).innerHTML = 'id banco';
    row.insertCell(1).innerHTML = nome.value;
    row.insertCell(2).innerHTML = telefone.value;
    row.insertCell(3).innerHTML = email.value;
    row.insertCell(4).innerHTML = empresa.value;
    row.insertCell(5).innerHTML = endereco.value;
    row.insertCell(6).innerHTML = cidade.value;
    row.insertCell(7).innerHTML = estado.value;
    row.insertCell(8).innerHTML = '<i class="fas fa-trash-alt fa-lg" type="button" value = "Delete" onClick="Javacsript:deleteRow(this)"></i>';

}

function deleteRow(obj) {

    var index = obj.parentNode.parentNode.rowIndex;
    var table = document.getElementById("tabela");
    table.deleteRow(index);

}