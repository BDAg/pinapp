$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
});

document.querySelectorAll('.table-produtos tr').forEach(function (elem) {
    if (parseFloat(window.getComputedStyle(elem).width) === parseFloat(window.getComputedStyle(elem.parentElement).width)) {
        elem.setAttribute('title', elem.textContent);
    }
});

function search() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("buscador");
    filter = input.value.toUpperCase();
    table = document.getElementById("tabela");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function sellItens() {
    var linhas = document.getElementById("tabela").rows.length;

    for (i = 1; i < linhas; i++) {
        var conteudo = document.getElementById("tabela").rows[i].cells[2].innerText;

        var form = document.getElementById("exampleFormControlSelect1");
        var option = document.createElement("option");
        option.text = conteudo;
        form.add(option);
    }

}

function deleteSell() {
    var element = document.getElementsByTagName("option"), index;

    for (index = element.length - 1; index >= 0; index--) {
        element[index].parentNode.removeChild(element[index]);
    }

}

function sell(obj) {

    var r = confirm("Deseja vender o produto selecionado?");
    if (r == true) {

        var x = document.getElementById("exampleFormControlSelect1").selectedIndex;
        var y = document.getElementById("exampleFormControlSelect1").options;
        console.log("Index: " + y[x].index + " is " + y[x].text);
        var valueToFind = y[x].text;

        var table = document.getElementById("tabela");
        var rowCount = table.rows.length;

        for (var i = 0; i < rowCount; i++) {
            var row = table.rows[i];
            var text = row.cells[2].innerText;
            if (text.indexOf(valueToFind) != -1) {
                table.deleteRow(i);
            }
        }

    }

}

(function () {
    'use strict';
    window.addEventListener('load', function () {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

function addRow() {

    var usuario = document.getElementById("validationCustom01");
    var nome = document.getElementById("validationCustom02");
    var descricao = document.getElementById("validationCustom03");
    var preco = document.getElementById("validationCustom04");
    var peso = document.getElementById("validationCustom05");
    var fabricacao = document.getElementById("validationCustom06");
    var validade = document.getElementById("validationCustom07");
    var quantidade = document.getElementById("validationCustom08");

    var table = document.getElementById("tabela");

    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);

    row.insertCell(0).innerHTML = 'id banco';
    row.insertCell(1).innerHTML = usuario.value;
    row.insertCell(2).innerHTML = nome.value;
    row.insertCell(3).innerHTML = descricao.value;
    row.insertCell(4).innerHTML = preco.value;
    row.insertCell(5).innerHTML = peso.value;
    row.insertCell(6).innerHTML = fabricacao.value;
    row.insertCell(7).innerHTML = validade.value;
    row.insertCell(8).innerHTML = quantidade.value;
    row.insertCell(9).innerHTML = '<i class="fas fa-trash-alt fa-lg" type="button" value = "Delete" onClick="Javacsript:deleteRow(this)"></i>';

}

function deleteRow(obj) {

    var index = obj.parentNode.parentNode.rowIndex;
    var table = document.getElementById("tabela");
    table.deleteRow(index);
}

function getAll() {
    const corpoTabela = document.querySelector("[data-conteudo-tabela]")

    fetch('https://api-pinapp.herokuapp.com/produtos')
        .then(res => res.json())
        .then(states => {
            for (const state of states) {
                corpoTabela.innerHTML +=
                    `<tr>
                    <td>${state.id}</td>
                    <td>${state.usuario_id}</td>
                    <td>${state.title}</td>
                    <td>${state.description}</td>
                    <td>${state.price_by_kg}</td>
                    <td>${state.weight_kg}</td>
                    <td>${state.fabrication}</td>
                    <td>${state.validity}</td>
                    <td>${state.quantity_stock}</td>
                    <td>
                        <i class="fas fa-edit fa-lg"></i>
                        <i class="fas fa-trash-alt fa-lg"></i>
                    </td>`
            }
        })
        .catch(err => console.log(err))
}
getAll();