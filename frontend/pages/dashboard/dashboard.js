$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
});

var ctx = document.getElementById('myChart').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: ['Trigo', 'Ovos', 'Açucar', 'Chocolate', 'Manteiga', 'Leite', 'Farinha'],
        datasets: [{
            label: 'Consumo de Máteria Prima',
            backgroundColor: ['#FAE23F', '#E3BB30', '#FABF41', 'E39254', '#FA8A50', '#E39A30', '#FF5B26'],
            maxBarThickness: 80,
            borderColor: 'rgb(255, 99, 132)',
            data: [5, 10, 5, 2, 20, 30, 45]
        }]
    },

    // Configuration options go here
    options: {}
});