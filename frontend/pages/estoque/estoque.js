$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
});

document.querySelectorAll('.table-estoque tr').forEach(function (elem) {
    if (parseFloat(window.getComputedStyle(elem).width) === parseFloat(window.getComputedStyle(elem.parentElement).width)) {
        elem.setAttribute('title', elem.textContent);
    }
});

function search() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("buscador");
    filter = input.value.toUpperCase();
    table = document.getElementById("tabela");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
            txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

(function () {
    'use strict';
    window.addEventListener('load', function () {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

function addRow() {

    var fornecedor = document.getElementById("validationCustom01");
    var descricao = document.getElementById("validationCustom02");
    var marca = document.getElementById("validationCustom03");
    var quantidade = document.getElementById("validationCustom04");
    var unidade = document.getElementById("validationCustom05");
    var cor = document.getElementById("validationCustom06");
    var embalagem = document.getElementById("validationCustom07");
    var unitario = document.getElementById("validationCustom08");
    var total = document.getElementById("validationCustom09");

    var table = document.getElementById("tabela");

    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);

    row.insertCell(0).innerHTML = 'id banco';
    row.insertCell(1).innerHTML = fornecedor.value;
    row.insertCell(2).innerHTML = descricao.value;
    row.insertCell(3).innerHTML = marca.value;
    row.insertCell(4).innerHTML = quantidade.value;
    row.insertCell(5).innerHTML = unidade.value;
    row.insertCell(6).innerHTML = cor.value;
    row.insertCell(7).innerHTML = embalagem.value;
    row.insertCell(8).innerHTML = unitario.value;
    row.insertCell(9).innerHTML = total.value;
    row.insertCell(10).innerHTML = '<i class="fas fa-trash-alt fa-lg" type="button" value = "Delete" onClick="Javacsript:deleteRow(this)"></i>';

}

function deleteRow(obj) {

    var index = obj.parentNode.parentNode.rowIndex;
    var table = document.getElementById("tabela");
    table.deleteRow(index);

}