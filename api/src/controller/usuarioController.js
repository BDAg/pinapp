const connection = require('../database/connection');

const crypto = require('crypto');


module.exports= {
  // GET FUNCTION
  async index (req,res) {
    const usuarios = await connection('usuarios').select('*');
    console.log(`Todas os usuários foram listados`);
    return res.json(usuarios);
    },
  // POST FUNCTION
  async create (req, res) {
    const { name, email, empresa_nome, addres, city, uf } = req.body;

  //Gerar id aleatorios com crypto (4 bits) hexadeciamis
    const id = crypto.randomBytes(3).toString('HEX');

  //operações com database
    await connection('usuarios').insert({
        id,
        name,
        email,
        empresa_nome,
        addres,
        city,
        uf
    })
    console.log(`O USUARIO:${name} foi cadastrado com sucesso`);
    /**
     * Somente o id será retornado para
     * o usuário, para que ele possa receber
     * este id, e guardar ele para fazer
     * o login na aplicação
     */
    return res.json({ id });
  }
};