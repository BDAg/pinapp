const connection = require('../database/connection');

module.exports = {

    async getAll(req, res, next) {

        try {

            const materias = await connection('materias_primas').select('*');

            console.log("Todas as matérias primas foram listadas\n");

            return res.send(materias);

        } catch (error) {

            next(error);
        }

    },

    async create(req, res, next) {

        try{

            const {
                fornecedor_id,
                descricao,
                marca,
                quantidade_estoque,
                unidade_medida,
                cor,
                quantidade_embalagem,
                valor_unitario,
                valor_total
            } = req.body;

            const materias = await connection('materias_primas').insert({
                fornecedor_id,
                descricao,
                marca,
                quantidade_estoque,
                unidade_medida,
                cor,
                quantidade_embalagem,
                valor_unitario,
                valor_total               
            });
            
            console.log("Uma nova materia prima foi criada\n");

            return res.send(materias);

        } catch(error) {

            next(error);
        }

    },

    async update(req, res, next) {

        try {

            const {id} = req.params;

            const {
                fornecedor_id,
                descricao,
                marca,
                quantidade_estoque,
                unidade_medida,
                cor,
                quantidade_embalagem,
                valor_unitario,
                valor_total
            } = req.body;

            await connection('materias_primas').update({
                fornecedor_id,
                descricao,
                marca,
                quantidade_estoque,
                unidade_medida,
                cor,
                quantidade_embalagem,
                valor_unitario,
                valor_total  
            }).where({id});

            console.log(`Materia Prima ${id} foi modificada\n`);

            return res.send();

        } catch(error) {

            next(error);
        }

    },

    async delete(req, res, next) {

        try {

            const {id} = req.params;

            await connection('materias_primas').where({id}).delete();

            console.log(`Materia Prima ${id} foi deletada\n`);

            return res.status(204).send();

        } catch(error) {

            next(error);
        }

    }

}