const connection = require('../database/connection');

module.exports = {

    async getAll (req, res, next) {

        try{

            const fornecedores = await connection('fornecedores').select('*');

            console.log("Todos os fornecedores foram listados");

            return res.json(fornecedores);

        } catch(error) {

            next(error)

        }

    },

    async create(req, res, next) {

        try {

            const {
                nome,
                telefone,
                email,
                empresa_nome,
                address,
                city,
                uf
            } = req.body;

            const fornecedor = await connection('fornecedores').insert({
                nome,
                telefone,
                email,
                empresa_nome,
                address,
                city,
                uf
            });

            console.log(`Fornecedor: ${empresa_nome} foi criado com sucesso`);

            return res.send(fornecedor);

        } catch(error) {
            next(error);
        }

    },

    async update(req, res, next) {

        try{

            const {id} = req.params;

            const {
                nome,
                telefone,
                email,
                empresa_nome,
                address,
                city,
                uf
            } = req.body;            

            await connection('fornecedores').update({
                nome,
                telefone,
                email,
                empresa_nome,
                address,
                city,
                uf
            }).where({id});

            console.log("Update Feito");

            return res.send();

        } catch(error) {

            next(error);
        }

    },

    async delete(req, res, next) {

        try {

            const {id} = req.params;

            await connection('fornecedores').where({id}).delete();

            console.log(`Fornecedor ${id} deletado \n`);

            return res.status(204).send();

        } catch(error) {

            next(error);
        }

    }

}