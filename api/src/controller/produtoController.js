const connection = require('../database/connection');

module.exports = {
    async index (req, res) {
        const produtos = await connection('produtos').select('*');
        console.log(`Todos os produtos prontos foram listados`)
        return res.json(produtos);
    },

    async create (req, res, next) {
        try {

            //Para session do usuario
            const usuario_id = req.headers.authorization;

            const { 
            title, 
            description, 
            price_by_kg,
            weight_kg,
            fabrication,
            validity,
            quantity_stock
        } = req.body;

            

            const [id] = await connection('produtos').insert({
                usuario_id,
                title, 
                description, 
                price_by_kg,
                weight_kg,
                fabrication,
                validity,
                quantity_stock
    
            });
            console.log(`Criado novo ${title} em produtos`);
            return res.json({ id });

        } catch (error) {
            
            next(error)
        }
      },

      async update(req, res, next) {
          
        try {

            const usuario_id = req.headers.authorization;

            
            const { price_by_kg } = req.body
            const { id } = req.params
            
            const produto = await connection('produtos')
            .update({ price_by_kg })
            .where({ id })
            .select('usuario_id')

            return res.send()

        } catch (error) {
            next(error)
        }
    },

        //Deleta por ID
      async delete_id (req, res, next) {
        const { id } = req.params;
        const usuario_id = req.headers.authorization;

        const produto = await connection('produtos')
          .where('id',id)
          .select('usuario_id')
          .first();
        
        
        
        try {

            if ( produto.usuario_id != usuario_id ) {
                return res.status(401).json({ erro:'Operation not permitted.  ' })    
              }

            await connection('produtos').where('id',id).delete();
            console.log(`O produto_id:${id} foi deletado do banco pelo usuario_id:${usuario_id} com sucesso `)
            return res.status(204).send();
        
        } catch (error) {
            next(error)
        }
        
      },

      //Deleta TODOS 
      async delete_all (req, res, next) {
         await connection('produtos')
          .delete('*')
          return res.status(204).json({ message: 'todos os produtos foram deletados do banco'});
    
        
      }



}