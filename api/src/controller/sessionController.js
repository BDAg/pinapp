const connection = require('../database/connection');

module.exports = {
    async create(req, res) {
        const { id } = req.body
        
        const usuario = await connection('usuarios')
            .where('id',  id)
            .select('name')
            .first();

        if (!usuario) {
            return res.status(400).json({ error:'No user found with this ID' });   
        }
        return res.json(usuario);
    
    }
}
