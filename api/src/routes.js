const express = require('express');
const app = express();
const routes = express.Router();

const usuarioController = require('./controller/usuarioController');
const sessionController = require('./controller/sessionController');
const produtoController = require('./controller/produtoController');
const fornecedorController = require('./controller/fornecedorController');
const materiaPrimaController = require('./controller/materiaPrimaController');




app.use((req, res, next ) =>{
    const erro = new Error('Não encontrado');
    erro.status = 404;
    next(erro);  
  });

app.use((error, req, res, next) => {
res.status(error.status || 500);
return res.send ({
    erro: {
    mensagem: error.message
    }
});
});

/*========= USUARIOS e session (autenticação) ========= */
routes.get('/usuarios', usuarioController.index);
routes.post('/form-usuario', usuarioController.create);
routes.post('/session', sessionController.create);
/*====================================================== */
/*====================== PRODUTOS PRONTOS ====================== */
/*====================================================== */
routes.get('/produtos', produtoController.index);
routes.post('/produtos', produtoController.create);
routes.put('/produtos/:id', produtoController.update);
routes.delete('/produtos/:id', produtoController.delete_id);
// routes.delete('/produtos', produtoController.delete_all);
/*====================================================== */
/*====================== FORNECEDORES ====================== */
/*====================================================== */
routes.get('/fornecedores', fornecedorController.getAll);
routes.post('/fornecedores', fornecedorController.create);
routes.put('/fornecedores/:id', fornecedorController.update);
routes.delete('/fornecedores/:id', fornecedorController.delete);
/*====================================================== */
/*====================== MATERIAS PRIMAS ====================== */
/*====================================================== */

routes.get('/materias_primas', materiaPrimaController.getAll);
routes.post('/materias_primas', materiaPrimaController.create);
routes.put('/materias_primas/:id', materiaPrimaController.update);
routes.delete('/materias_primas/:id', materiaPrimaController.delete);

module.exports = routes;