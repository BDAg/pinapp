//
exports.up = function(knex) {
    return knex.schema.createTable('usuarios', function (table) {
        table.string('id').primary();
        table.string('name').notNullable();
        table.string('email').notNullable();
        table.string('empresa_nome', 50).notNullable();
        table.string('addres', 50).notNullable();
        table.string('city', 50).notNullable();
        table.string('uf', 2).notNullable();
    
      })
};
//down for erro at up
exports.down = function(knex) {
    return knex.schema.dropTable('usuarios');
};
