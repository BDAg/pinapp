
exports.up = function(knex) {
    
    return knex.schema.createTable('fornecedores', function(table) {
        table.increments();
        table.string('nome').notNullable();
        table.string('telefone').notNullable();
        table.string('email').notNullable();
        table.string('empresa_nome').notNullable();
        table.string('address').notNullable();
        table.string('city').notNullable();
        table.string('uf').notNullable();

    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('fornecedores');
};
