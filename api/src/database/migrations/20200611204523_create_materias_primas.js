
exports.up = function(knex) {
    return knex.schema.createTable('materias_primas', function(table) {
        table.increments();
        table.string('fornecedor_id').notNullable();
        table.foreign('fornecedor_id').references('id').inTable('fornecedores');
        table.string('descricao').notNullable();
        table.string('marca').notNullable();
        table.decimal('quantidade_estoque').notNullable();
        table.string('unidade_medida').notNullable();
        table.string('cor');
        table.string('quantidade_embalagem').notNullable();
        table.decimal('valor_unitario').notNullable();
        table.decimal('valor_total').notNullable();
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('materias_primas');
};
