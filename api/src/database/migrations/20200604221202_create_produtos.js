
exports.up = function(knex) {
  return knex.schema.createTable('produtos', function (table) {
      table.increments();
      //relacionamento
      table.string('usuario_id').notNullable();
      // chave estrangeira - campo - tabela
      table.foreign('usuario_id').references('id').inTable('usuarios');
      table.string('title').notNullable();
      table.string('description', 90).notNullable();
      table.decimal('price_by_kg').notNullable();
      table.decimal('weight_kg').notNullable();
      table.date('fabrication').notNullable();
      table.date('validity').notNullable();
      table.integer('quantity_stock').notNullable();
    })
};

exports.down = function(knex) {
  return knex.schema.dropTable('produtos')
};
