const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const port = process.env.PORT || 8080;
const routes = require('./routes');

const app = express();
  
app.use(morgan('dev'));

app.use(cors());
app.use(express.json());
app.use(routes);


app.listen(port,() => {
    console.log(`servidor rodando na porta: ${port}...\n\n`)
});
