# PINAP

## Documentação 

### Sobre o projeto

O app de gestão de estoque, controle de compras com fornecedores e venda de produtos, com design intuitivo e acessível.

[Saber mais.](https://gitlab.com/BDAg/pinapp/wikis/Documentação/MVP)

[Cronograma.](https://gitlab.com/BDAg/pinapp/wikis/Documentação/Cronograma)

[Mapa de definição tecnológica.](https://gitlab.com/BDAg/pinapp/wikis/Documentação/Mapa-de-Conhecimento)

[Integrantes da equipe.](https://gitlab.com/BDAg/pinapp/wikis/Equipe)

### Andamento do Projeto

- [x] [Definições de Projeto](https://gitlab.com/BDAg/pinapp/wikis/Documentação/MVP)
- [X] [Documentação da Solução]()
- [X] Desenvolvimento da Solução (Sprint 1)
- [X] Desenvolvimento da Solução (Sprint 2)
- [X] Desenvolvimento da Solução (Sprint 3)
- [ ] Fechamento do Projeto

